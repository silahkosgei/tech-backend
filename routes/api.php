<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Status\StatusController;
use App\Http\Controllers\Api\Reports\ReportsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function(){
    Route::post('auth/register', 'register');
    Route::post('auth/login', 'login');
});
Route::middleware('auth:sanctum')->group(function () {
    $routes_path = base_path('routes/api');
    if(file_exists($routes_path)) {
        $route_files = File::allFiles(base_path('routes/api'));
        foreach ($route_files as $file) {
            $path = $file->getPath();
            $file_name = $file->getFileName();
            $prefix = str_replace($file_name, '', $path);
            $prefix = str_replace($routes_path, '', $prefix);
            $file_path = $file->getPathName();
            $this->route_path = $file_path;
            $arr = explode('/', $prefix);
            $len = count($arr);
            $main_file = $arr[$len - 1];
            $arr = array_map('ucwords', $arr);
            $arr = array_filter($arr);
            $ext_route = str_replace('user.route.php', '', $file_name);
            if($main_file.'.route.php' === $ext_route)
                $ext_route = str_replace($main_file.'.', '.', $ext_route);
            $ext_route = str_replace('.route.php', '', $ext_route);
            if ($ext_route)
                $ext_route = '/' . $ext_route;
            $prefix = strtolower($prefix . $ext_route);
            Route::group(['prefix' => $prefix], function () {
                require $this->route_path;
            });
        }
    }
    // Admin routes
    Route::middleware(['auth', 'Admin'])->group(function () {
        Route::get('users', [App\Http\Controllers\Controller::class, 'getUsers']);
        Route::resource('status', StatusController::class)->except(['update', 'edit']);;
        Route::controller(StatusController::class)->group(function(){
            Route::get('status', [StatusController::class,'index']);
            Route::get('status/list/{paginate?}', [StatusController::class,'index']);
            Route::get('status/{id}', [StatusController::class,'show']);
            Route::post('status/{id?}', [StatusController::class,'store']);
            Route::post('status/delete/{id}', [StatusController::class,'destroy']);
        });
    });
});
