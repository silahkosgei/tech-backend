<?php
use App\Http\Controllers\Api\Auth\AuthController;

Route::post('logout', [AuthController::class, 'logout']);
Route::get('user', [AuthController::class, 'loggedInUser']);
Route::post('update/password', [AuthController::class, 'updatePassword']);
