<?php
use App\Http\Controllers\Api\Tasks\TaskController;

Route::get('list/{status_name}', [TaskController::class, 'index']);
Route::post('store/{id?}', [TaskController::class, 'store']);
Route::get('get/{id}', [TaskController::class, 'getTask']);
Route::post('delete/{id}', [TaskController::class, 'deleteTask']);
Route::post('approve/{task_id}', [TaskController::class, 'approveTask']);
Route::post('assign/{task_id}', [TaskController::class, 'assignTask']);
Route::post('mark-complete/{task_id}', [TaskController::class, 'markComplete']);
