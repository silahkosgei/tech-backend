<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $statuses = [
            ['name' => 'pending'],
            ['name' => 'active'],
            ['name' => 'in-progress'],
            ['name' => 'completed'],
            ['name' => 'deleted'],
            ['name' => 'approved'],
        ];
        if (DB::table('statuses')->count() === 0) {
            DB::table('statuses')->insert($statuses);
        }
    }
}
