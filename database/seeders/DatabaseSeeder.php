<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        try {
            if (!\App\Models\User::where('email', '=', 'silakosy@gmail.com' )->exists()) {
                //test admin user
                  \App\Models\User::factory()->create([
                    'name' => 'Test admin',
                    'email' => 'silakosy@gmail.com',
                    'password' => Hash::make('password'),
                    'role' => 'admin',
                ]);
            }
            if (!\App\Models\User::where('email', '=', 'test@gmail.com' )->exists()) {
                \App\Models\User::factory()->create([
                    'name' => 'Test User',
                    'email' => 'test@gmail.com',
                    'password' => Hash::make('password'),
                    'role' => 'user',
                ]);
            }
        }   catch (\Exception $e) {
            echo $e->getMessage();
        }
         \App\Models\User::factory(5)->create();
        $this->call(StatusesTableSeeder::class);
    }
}
