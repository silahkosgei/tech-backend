<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "description",
        "due_date",
        "deleted_at",
        "status_id",
        "start_time",
        "end_time",
        "remarks",
    ];
    public function assignUser($user_id, $status_id, $start_time, $end_time,$due_date,$remarks) {
        $this->users()->attach($user_id, [
            'status_id' => $status_id,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'due_date' => $due_date,
            'remarks' => $remarks,
        ]);
    }
    public function users() {
        return $this->belongsToMany(User::class, 'task_users');
    }

    public function status() {
        return $this->belongsTo(Status::class);
    }

    public function approveTask()
    {
        $status = $this->status->name == 'completed'
            ? Status::where('name','approved')->first()
            : Status::where('name','active')->first();
        $this->status_id = $status->id;
        $this->save();
    }

    public function markComplete()
    {
        $completed_status = Status::where('name','completed')->first();
        $this->status_id = $completed_status->id;
        $this->save();
    }

    public function getTaskStatus($status)
    {
        return $this->status->name == $status;
    }

}
