<?php

namespace App\Repositories;
class ShRepository
{

    public static function getChartData($query,$type='stock',$fields=[],$date_field='created_at'){
        return GraphStatsRepository::getDrawables($query,$type,$fields,$date_field);
    }
}
