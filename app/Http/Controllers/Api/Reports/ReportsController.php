<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\Core\Post;
use App\Models\Core\Workspace;
use App\Models\Task;
use App\Models\TaskUser;
use App\Models\User;
use Carbon\Carbon;
use Iankibet\Shbackend\App\Repositories\ShRepository;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function getReport() {
        $user = \request()->user();
        $from = \request('from');
        $to = \request('to');
        $start = Carbon::createFromFormat('m/d/Y',$from)->startOfDay();
        $end = Carbon::createFromFormat('m/d/Y',$to)->endOfDay();
        $tasks = Task::whereBetween('created_at',[$start,$end]);
        if($user->role == 'admin') {
            $in_progress = $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%in-progress%');
            })->count();
            $completedTasks =  $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%completed%');
            })->count();
            $approvedTasks=  $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%approved%');
            })->count();
            $revisionTasks =  $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%revision%');
            })->count();
        } else {
            $tasks = TaskUser::whereBetween('created_at',[$start,$end])->where('user_id',$user->id);
            $in_progress = $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%in-progress%');
            })->count();
            $completedTasks =  $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%completed%');
            })->count();
            $approvedTasks=  $tasks->whereHas('status', function($query) {
                $query->where('name', 'like', '%approved%');
            })->count();
            $revisionTasks =  $tasks->count();
        }
        $tasks = $user->role =='admin' ? new Task() : TaskUser::where('user_id',$user->id);
        $stats = \App\Repositories\ShRepository::getChartData($tasks,'normal');
        return response([
            'stats' => $stats,
            'in_progress' => $in_progress,
            'completedTasks' => $completedTasks,
            'approvedTasks' => $approvedTasks,
            'revisionTasks' => $revisionTasks

        ]);
    }
    public function getTaskStatus($status) {
        $statuses = [
            'in_progress' => 1,
            'completed' => 2,
            'approved' => 3,
            'revision' => 4
        ];
        return $statuses[$status];
    }

}
