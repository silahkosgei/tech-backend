<?php

namespace App\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StatusController extends Controller
{
    public function index($paginate = null)
    {
        $statuses = Status::where('state','active')
            ->orderBy('id','desc');
        $statuses = $paginate !== null ? $statuses->get()->toArray() : $statuses->paginate(10);
        return response([
            'status'=>'success',
            'data'=>$statuses
        ]);
    }

    function store($id = null){
            $rules = [
                'name'=>'required|unique:statuses',
            ];
        $data = request()->all();
        $valid = Validator::make($data,$rules);
        if (count($valid->errors())) {
            return response([
                'status' => 'failed',
                'errors' => $valid->errors()
            ], 422);
        }
        $status = $id == null ?
            Status::create([
                'name'=>request('name'),
            ])
            : tap(Status::find($id))->update([
                'name'=>request('name'),
            ]);
        return response([
            'status'=>'success',
            'data'=>$status
        ]);
    }
    public function show($status_id)
    {
        $status = Status::find($status_id);
        return response()->json($status);
    }

    public function destroy($status_id) {
        $status = Status::find($status_id);
        $status->state = 'deleted';
        $status->save();
        return response([
                'status'=>'success',
                'message'   =>'Status deleted successfully'
            ]);
    }
}
