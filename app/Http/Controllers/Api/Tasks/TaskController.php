<?php

namespace App\Http\Controllers\Api\Tasks;

use App\Http\Controllers\Controller;
use App\Models\Status;
use App\Models\Task;
use App\Models\TaskUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function index($status_name)
    {
        $user = \request()->user();
        $tasks = Task::join('statuses','statuses.id','=','tasks.status_id')
            ->where('statuses.name',$status_name)
            ->select('tasks.*')
            ->orderBy('tasks.id','desc');
        $tasks = $user->role == 'admin'
            ? $tasks
            : // get tasks assigned to user
            $tasks->join('task_users','task_users.task_id','=','tasks.id')
                ->where('task_users.user_id',$user->id)
                ->select('tasks.*');
        $tasks = $tasks->paginate(10);
        return response([
            'status'=>'success',
            'data'=>$tasks
        ]);
    }

    public function store($id = null)
    {
        $rules = [
            'name'=>'required',
            'description'=>'required',
            'due_date'=>'required',
            'status_id'=>'required',
        ];
        $data = request()->all();
        if ($id != null) {
            unset($rules['status_id']);
            $task = Task::find($id);
            $data['status_id'] = $task->status_id;

        }
        if (isset($data['due_date']) && $data['due_date'] != null)
            $data['due_date'] = Carbon::parse(\request()->due_date)->format('Y-m-d H:i:s');
        \request()->merge($data);
        $valid = Validator::make($data,$rules);
        if (count($valid->errors())) {
            return response([
                'status' => 'failed',
                'errors' => $valid->errors()
            ], 422);
        }
        $task = $id == null ?
            Task::create([
                'name'=>request('name'),
                'status_id'=>request('status_id'),
                'description'=>request('description'),
                'due_date'=>request('due_date')
            ])
            : tap(Task::find($id))->update([
                'name'=>request('name'),
                'status_id'=>request('status_id'),
                'description'=>request('description'),
                'due_date'=>request('due_date')
            ]);
            return response([
                'status'=>'success',
                'data'=>$task
            ]);
    }

    public function getTask($id)
    {
        $task = Task::find($id);
        if ($task) {
            return response([
                'status'=>'success',
                'data'=>$task
            ]);
        }
        return response([
            'status'=>'failed',
            'errors'=>['id'=>['Task not found']]
        ],422);
    }

    public function deleteTask($id)
    {
        $task = Task::find($id);
        if ($task) {
            $deleted_status = Status::where('name','deleted')->first();
            $task->deleted_at = Carbon::now();
            $task->status_id = $deleted_status->id;
            $task->save();
            return response([
                'status'=>'success',
                'data'=>$task
            ]);
        }
        return response([
            'status'=>'failed',
            'errors'=>['id'=>['Task not found']]
        ],422);
    }

    public function assignTask($task_id)
    {
        $rules = [
            'user_id'=>'required',
            'start_time'=>'required',
            'end_time'=>'required',
        ];
        $data = request()->all();
        $data['user_id'] = request()->user_id;
        if (isset($data['start_time']) && $data['start_time'] != null || isset($data['end_time']) && $data['end_time'] != null)
            $data['start_time'] = Carbon::parse(request()->due_date)->format('Y-m-d H:i:s');
            $data['end_time'] = Carbon::parse(request()->due_date)->format('Y-m-d H:i:s');
        $in_progress_status = Status::where('name','in-progress')->first();
        $data['status_id'] = $in_progress_status->id;
        request()->merge($data);
        $valid = Validator::make($data,$rules);
        if (count($valid->errors())) {
            return response([
                'status' => 'failed',
                'errors' => $valid->errors()
            ], 422);
        }
        $task = Task::find($task_id);
        if ($task) {
            $task->assignUser(
                request('user_id'),
                request('status_id'),
                request('start_time'),
                request('end_time'),
                $task->due_date,
                request('remarks')
            );
            $task->status_id = $in_progress_status->id;
            $task->save();
            return response([
                'status' => 'success',
                'data' => $task
            ]);
        }
        return response([
            'status'=>'failed',
            'errors'=>['id'=>['Task not found']]
        ],422);

    }

    public function approveTask($task_id)
    {
        $task = Task::find($task_id);
        if ($task) {
            if (request('remarks')) {
                $userTask = TaskUser::where('task_id',$task_id)->first();
                $userTask->remarks = $userTask->remarks ?? '' . "\r\n" . request('remarks');
                $userTask->save();
            }

            $task->approveTask();
            return response([
                'status' => 'success',
                'data' => $task
            ]);
        }

    }

    public function markComplete($task_id)
    {
        $task = Task::find($task_id);
        if ($task) {
            $task->markComplete();
            return response([
                'status' => 'success',
                'data' => $task
            ]);
        }

    }
}
