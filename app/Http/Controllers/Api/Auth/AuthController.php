<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(){
        $data = request()->all();
        $rules = [
            'email'=>'required',
            'password'=>'required'
        ];
        $valid = Validator::make($data,$rules);
        if (count($valid->errors())) {
            return response([
                'status' => 'failed',
                'errors' => $valid->errors()
            ], 422);
        }
        $email = request('email');
        $password = request('password');
        if(Auth::attempt(['email'=>$email,'password'=>$password])){
            $token = request()->user()->createToken('api_token_at_'.now()->toDateTimeString());
            return response([
                'status'=>'success',
                'access_token'=>$token->plainTextToken,
                'user'=>request()->user()
            ]);
        }
        return response([
            'status'=>'failed',
            'errors'=>['email'=>['Invalid email or password']]
        ],422);
    }

    public function register(){
        $rules = [
            'email'=>'required|unique:users',
            'name'=>'required',
            'password'=>'required|confirmed',
        ];
        $data = request()->all();
        $valid = Validator::make($data,$rules);
        if (count($valid->errors())) {
            return response([
                'status' => 'failed',
                'errors' => $valid->errors()
            ], 422);
        }
        $password = \request('password');
        User::create([
            'name'=>request('name'),
            'email'=>request('email'),
            'password'=>Hash::make($password),
        ]);
        if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
            $token = request()->user()->createToken('api_token_at_'.now()->toDateTimeString());
            return response([
                'status'=>'success',
                'access_token'=>$token->plainTextToken,
                'user'=>request()->user()
            ]);
        }
    }
    public function logout() {
        $user = Auth::user();

        $user->currentAccessToken()->delete();

        return response([
            'user' => 'User logged out successfully.',
        ]);
    }

    public function loggedInUser () {

        $user = Auth::user();
        return response([
            'user' => $user,
        ]);
    }
    public function updatePassword(){
        $data = \request()->all();
        $rules = [
            'current_password'=>'required',
            'password'=>'required|confirmed',
        ];
        $valid = Validator::make($data,$rules);
        if (count($valid->errors())) {
            return response([
                'status' => 'failed',
                'errors' => $valid->errors()
            ], 422);
        }
        $user = \request()->user();
        if(!Hash::check(\request('current_password'),$user->password)){
            return response([
                'status' => 'failed',
                'errors' => ['current_password'=>['Current password incorrect']]
            ], 422);
        }
        $password = request('password');
        $user->password = Hash::make($password);
        $user->update();
        return [
            'status'=>'success',
            'message'=>'password updated successfully'
        ];
    }
}
